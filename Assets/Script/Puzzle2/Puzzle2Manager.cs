﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;

public class Puzzle2Manager : MonoBehaviour
{
    public Puzzle2 preguta1;
    public Puzzle2 preguta2;
    public int estado = 0;
    private int rnd;
    public SpriteRenderer cerrado;
    public SpriteRenderer medio_abierto;
    public SpriteRenderer abierto;
    public GameObject FinalBueno;
    public GameObject FinalMalo;
    public GameObject Inventario;
    public GameObject llave;
    public GameObject mapa;
    public bool changeQuestion = false;
    public Text llaves_text;
    public Text mapas_text;
    public Text helps_text;
    public Text ganzua_text;
    public Text nombre_cosa;
    public Text Resultado;
    public string SceneName = "";
    public Button BackMenu;
    public Button siguiente;
    public Button Reintentar;
    public int ganzuas_en_Inventario;
    public bool assertsCheck;
    // Start is called before the first frame update
    void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        BackMenu.onClick.AddListener(() => ChangeName(BackMenu));
        siguiente.onClick.AddListener(() => ChangeName(siguiente));
        Reintentar.onClick.AddListener(() => ChangeName(Reintentar));
        //this.ganzuas_en_Inventario = GeneralManager.instance.GetGanz();
    }

    // Update is called once per frame
    void Update()
    {
        if(estado == 1 && this.changeQuestion)
        {
            if(preguta1.respuesta_estado == 1)
            {
                this.cerrado.gameObject.SetActive(false);
                this.medio_abierto.gameObject.SetActive(true);
            }
            this.preguta1.fondo_negro.gameObject.SetActive(false);
            this.preguta1.gameObject.SetActive(false); 
            this.preguta2.fondo_negro.gameObject.SetActive(true);
            this.preguta2.gameObject.SetActive(true);
            this.changeQuestion = false;
        }
        if (estado == 2 && this.changeQuestion)
        {
            this.preguta2.fondo_negro.gameObject.SetActive(false);
            this.preguta2.gameObject.SetActive(false);
            this.changeQuestion = false;
            if (preguta2.respuesta_estado == 1)
            {
                if(preguta1.respuesta_estado == 1)
                {
                    GetRadomObject();
                    this.medio_abierto.gameObject.SetActive(false);
                    this.abierto.gameObject.SetActive(true);
                    this.FinalBueno.SetActive(true);
                    this.Resultado.text = "El cofre se abrió";
                    this.Resultado.gameObject.SetActive(true);
                }
                this.cerrado.gameObject.SetActive(false);
                this.medio_abierto.gameObject.SetActive(true);
                this.FinalMalo.SetActive(true);
                this.Resultado.text = "No pudiste abrir el cofre";
                this.Resultado.gameObject.SetActive(true);
            }
            else if(preguta1.respuesta_estado == 1)
            {
                this.cerrado.gameObject.SetActive(false);
                this.medio_abierto.gameObject.SetActive(true);
                this.FinalMalo.SetActive(true);
                this.Resultado.text = "No pudiste abrir el cofre";
                this.Resultado.gameObject.SetActive(true);                
            }
            else
            {
                this.cerrado.gameObject.SetActive(true);
                this.FinalMalo.SetActive(true);
                this.Resultado.text = "No pudiste abrir el cofre";
                this.Resultado.gameObject.SetActive(true);
                if (this.ganzuas_en_Inventario > 0)
                {
                    this.nombre_cosa.text = "¿Quieres usar una ganzua para volver a intentarlo?";
                    this.nombre_cosa.gameObject.SetActive(true);
                    //GeneralManager.instance.DisGanz();
                    this.Reintentar.gameObject.SetActive(true);
                }
            }
        }
    }
    public void GetRadomObject()
    {
        rnd = Random.Range(0, 2);
        if(rnd == 0)
        {
            this.llave.gameObject.SetActive(true);
            this.nombre_cosa.gameObject.SetActive(true);
            this.nombre_cosa.text = "Ganaste una llave oxidada";
            GeneralManager.instance.AumLlaves();
            this.llaves_text.text = GeneralManager.instance.GetLaves().ToString();
        }
        else if(rnd == 1)
        {
            this.mapa.gameObject.SetActive(true);
            this.nombre_cosa.gameObject.SetActive(true);
            this.nombre_cosa.text = "Ganaste un mapa Antiguo";
            GeneralManager.instance.AumMapa();
            this.mapas_text.text = GeneralManager.instance.GetMapa().ToString();
        }
        
    }
    public void ShowInventario()
    {
        this.helps_text.text = GeneralManager.instance.GetHelp().ToString();
        this.ganzua_text.text = GeneralManager.instance.GetGanz().ToString();
        this.Inventario.SetActive(true);
        this.FinalBueno.SetActive(false);
        this.FinalMalo.SetActive(false);
        this.nombre_cosa.gameObject.SetActive(false);
        this.Resultado.gameObject.SetActive(false);
    }
    public void Cerrar()
    {
        this.Inventario.SetActive(false);
        this.FinalBueno.SetActive(true);
        this.nombre_cosa.gameObject.SetActive(true);
        this.Resultado.gameObject.SetActive(true);
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
    public void ChangeName(Button boton)
    {
        if (boton.gameObject.name == "BackMenu")
        {
            SceneName = "MainMenu";
            ActivateLoadingCanvas();
        }
        else if (boton.gameObject.name == "Siguiente")
        {
            GeneralManager.instance.AumIndex(4);
            SceneName = "PantallaCarga";
            ActivateLoadingCanvas();
        }
        else if (boton.gameObject.name == "Reintentar")
        {
            SceneName = "Puzzle2";
            ActivateLoadingCanvas();
        }
    }
}
