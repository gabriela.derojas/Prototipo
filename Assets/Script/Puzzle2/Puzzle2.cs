﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle2 : MonoBehaviour
{
    public Image bar;
    public float fill = 1;
    public BotonesOpciones[] botones;
    public Puzzle2Manager manager;
    public bool state = false;
    public GameObject fondo_negro;
    public bool responder = false;
    public int respuesta_estado = 0;
    // Start is called before the first frame update
    void Awake()
    {
        GetRandomID();
    }

    // Update is called once per frame
    void Update()
    {
        this.fill = Mathf.Clamp01(this.fill - 0.0005f);
        this.bar.fillAmount = this.fill;
        if(this.state)
        {
            manager.estado++;
            this.state = false;
        }
        if(this.bar.fillAmount == 0)
        {
            this.manager.changeQuestion = true;
            if(!this.responder)
            {
                manager.estado++;
            } 
        }
    }
    
    public void GetRandomID()
    {
        botones[0].ID = RandomNumber();
        botones[1].ID = RandomNumber();
        botones[2].ID = RandomNumber();
        botones[3].ID = RandomNumber();
        while(botones[0].ID == botones[1].ID)
        {
            botones[1].ID = RandomNumber();
        }
        while(botones[2].ID == botones[0].ID || botones[2].ID == botones[1].ID)
        {
            botones[2].ID = RandomNumber();
        }
        while(botones[3].ID == botones[0].ID || botones[3].ID == botones[1].ID || botones[3].ID == botones[2].ID)
        {
            botones[3].ID = RandomNumber();
        }
    }
    public int RandomNumber()
    {
        int rnd = Random.Range(1, 5);
        return rnd;
    }
}
