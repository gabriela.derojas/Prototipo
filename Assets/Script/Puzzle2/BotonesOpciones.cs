﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonesOpciones : MonoBehaviour
{
    public Puzzle2 pregunta;
    public int ID;
    public bool respuesta_state;
    public Text texto;
    public Image color;
    public bool check = true;
    public Button boton;
    public Puzzle2Manager manager;
    // Start is called before the first frame update
    void Start()
    {
        this.boton = this.gameObject.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if(check)
        {
            if(this.manager.estado == 0)
            {
                if (this.ID == 1)
                {
                    this.respuesta_state = true;
                    this.texto.text = "Bartolomé Díaz";
                }
                else if (this.ID == 2)
                {
                    this.texto.text = "Cristóbal Colón";
                }
                else if (this.ID == 3)
                {
                    this.texto.text = "Enrique el Navegante";
                }
                else if (this.ID == 4)
                {
                    this.texto.text = "Vasco da Gama";
                }
            }
            else
            {
                if (this.ID == 1)
                {
                    this.respuesta_state = true;
                    this.texto.text = "Búsqueda de rutas comerciales";
                }
                else if (this.ID == 2)
                {
                    this.texto.text = "Expansión europea";
                }
                else if (this.ID == 3)
                {
                    this.texto.text = "Todas las opciones";
                }
                else if (this.ID == 4)
                {
                    this.texto.text = "Inicio de la colonización del mundo";
                }
            }
            
            this.check = false;
        }
        if (this.pregunta.responder)
        {
            OffButton();
        }
    }
    public void ShowRespuesta()
    {
        if(this.respuesta_state)
        {
            this.color.color = Color.green;
            this.pregunta.respuesta_estado = 1;
        }
        else
        {
            this.color.color = Color.red;
            this.pregunta.respuesta_estado = 0;
        }
        this.pregunta.responder = true;
        this.pregunta.state = true;
    }
    public void OffButton()
    {
        this.boton.enabled = false;
    }
}