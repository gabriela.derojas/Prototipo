﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonesTemporal : MonoBehaviour
{
    public Puzzle3 pregunta;
    public int ID;
    public bool respuesta_state;
    public Text texto;
    public Image color;
    public bool check = true;
    public Button boton;
    public Puzzle3Manager manager;
    // Start is called before the first frame update
    void Start()
    {
        this.boton = this.gameObject.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (check)
        {
            if(this.manager.estado == 0)
            {
                if (this.ID == 1)
                {
                    this.respuesta_state = true;
                    this.texto.text = "Sebastián el Cano";
                }
                else if (this.ID == 2)
                {
                    this.texto.text = "Cristóbal Colón";
                }
                else if (this.ID == 3)
                {
                    this.texto.text = "Hernán Cortez";
                }
                else if (this.ID == 4)
                {
                    this.texto.text = "Vasco Nuñes de Balboa";
                }
            } else if (this.manager.estado == 1)
            {
                if (this.ID == 1)
                {
                    this.respuesta_state = true;
                    this.texto.text = "Nueva ruta marítima para Portugal";
                }
                else if (this.ID == 2)
                {
                    this.texto.text = "Ruta alterna a América del Sur";
                }
                else if (this.ID == 3)
                {
                    this.texto.text = "Financiamiento europeo para las expediciones";
                }
                else if (this.ID == 4)
                {
                    this.texto.text = "Detener la expansión marítima vikinga";
                }
            }else if (this.manager.estado == 2)
            {
                if (this.ID == 1)
                {
                    this.respuesta_state = true;
                    this.texto.text = "Evitar conflictos por futuros descubrimientos";
                }
                else if (this.ID == 2)
                {
                    this.texto.text = "Defender el fortin Navidad";
                }
                else if (this.ID == 3)
                {
                    this.texto.text = "Mantener el comercio con las indias";
                }
                else if (this.ID == 4)
                {
                    this.texto.text = "Calmar el ánimo de guerra";
                }
            }
            this.check = false;
        }
        if(this.pregunta.responder)
        {
            OffButton();
        }
    }
    public void ShowRespuesta()
    {
        this.pregunta.state = true;
        if (this.respuesta_state)
        {
            this.color.color = Color.green;
            this.pregunta.manager.fill += 0.25f;
            this.pregunta.manager.preguntas_correctas++;
        }
        else
        {
            this.color.color = Color.red;
            this.pregunta.manager.fill -= 0.25f;
        }
        this.pregunta.responder = true;
    }
    public void OffButton()
    {
        this.boton.enabled = false;
    }
}
