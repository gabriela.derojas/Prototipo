﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Puzzle3Manager : MonoBehaviour
{

    public Puzzle3 pregunta1;
    public Puzzle3 pregunta2;
    public Puzzle3 pregunta3;
    public Text resultado_final;
    public bool changeQuestion = false;
    public int estado = 0;
    public Image bar;
    public float fill = 1;
    public int preguntas_correctas = 0;
    public Button siguiente;
    public string SceneName = "";
    public Text text_boton;
    // Start is called before the first frame update
    void Start()
    {
        siguiente.onClick.AddListener(() => ActivateLoadingCanvas());
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    // Update is called once per frame
    void Update()
    {
        this.bar.fillAmount = this.fill;
        if (estado == 1 && this.changeQuestion)
        {
            this.pregunta1.gameObject.SetActive(false);
            this.pregunta1.fondo_negro.gameObject.SetActive(false);
            this.pregunta2.gameObject.SetActive(true);
            this.pregunta2.fondo_negro.gameObject.SetActive(true);
            this.changeQuestion = false;
        }
        if (estado == 2 && this.changeQuestion)
        {
            this.pregunta2.gameObject.SetActive(false);
            this.pregunta2.fondo_negro.gameObject.SetActive(false);
            this.pregunta3.gameObject.SetActive(true);
            this.pregunta3.fondo_negro.gameObject.SetActive(true);
            this.changeQuestion = false;
        }
        if (estado == 3 && this.changeQuestion)
        {
            this.pregunta3.gameObject.SetActive(false);
            this.pregunta3.fondo_negro.gameObject.SetActive(false);
            if (this.preguntas_correctas >= 2)
            {
                this.resultado_final.text = "Felicidades, el escape fue un exito";
                this.siguiente.gameObject.SetActive(true);
                GeneralManager.instance.AumIndex(5);
                SceneName = "PantallaCarga";
                this.text_boton.text = "Siguiente";
            }
            else
            {
                this.resultado_final.text = "Te atraparon, el escape fracasó";
                this.siguiente.gameObject.SetActive(true);
                SceneName = "MainMenu";
                this.text_boton.text = "Menu";
            }
            this.resultado_final.gameObject.SetActive(true);
        }
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
