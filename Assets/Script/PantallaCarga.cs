﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PantallaCarga : MonoBehaviour
{
    public int escena;
    public GameObject tutoBarco;
    public GameObject tutoBuzo;
    public GameObject tutoPregunta;
    public GameObject tutoImagen;
    public GameObject spawnPoint;
    public GameObject spawnPointBarcoFail;
    public Text texto;
    public string SceneName = "";
    public Button cambioScena;
    // Start is called before the first frame update
    void Start()
    {
        this.escena = GeneralManager.instance.GetIndex();
        cambioScena.onClick.AddListener(() => ActivateLoadingCanvas());
        CargarTuto(escena);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void CargarTuto(int index)
    {
        GameObject temp;
        if(index == 1)
        {
            temp = Instantiate(tutoBarco, this.spawnPointBarcoFail.transform.position, this.spawnPointBarcoFail.transform.rotation);
            this.texto.text = "Deslice el dedo hacia arriba o abajo de la pantalla para mover el barco.";
            this.SceneName = "barquitoUwU";
        }
        else if(index == 2)
        {
            temp = Instantiate(tutoBuzo, this.spawnPoint.transform.position, this.spawnPoint.transform.rotation);
            this.texto.text = "Utilice los botones para mover verticalmente y arrastre para horizontal";
            this.SceneName = "SampleScene";
        }
        else if(index == 3)
        {
            this.tutoPregunta.SetActive(true);
            this.texto.text = "Elige la respuesta correcta, necesitas 2 para pasar";
            this.SceneName = "Puzzle2";
        }
        else if(index == 4)
        {
            this.tutoPregunta.SetActive(true);
            this.texto.text = "Elige la respuesta correcta, necesitas 3 para pasar";
            this.SceneName = "Puzzle3";
        }
        else if(index == 5)
        {
            this.tutoImagen.SetActive(true);
            this.texto.text = "Escribe el nombre del personaje en la imagen";
            this.SceneName = "Puzzles1";
        }
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
