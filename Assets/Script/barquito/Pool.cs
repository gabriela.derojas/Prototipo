﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public List<GameObject> pool;
    public GameObject objeto;
    public GameObject objetoFlip;
    public GameObject SpawnPoint;
    public float spawnRate;
    public int max;
    public bool waveIsDone = true;
    // Start is called before the first frame update
    void Start()
    {
        InstansiateObjects();
    }

    // Update is called once per frame
    void Update()
    {
        if (waveIsDone == true)
        {
            StartCoroutine(waveSpawner());
        }
    }
    public void InstansiateObjects()
    {
        GameObject temp;
        for(int i = 0; i < this.max; i++)
        {
            if(i%2 == 1)
            {
                temp = Instantiate(objeto, this.SpawnPoint.transform.position, this.SpawnPoint.transform.rotation);
                pool.Add(temp);
                temp.SetActive(false);
                temp.transform.SetParent(this.transform);
                temp.GetComponent<ObjetoMovimiento>().Initialice(this);
            }
            else
            {
                temp = Instantiate(objetoFlip, this.SpawnPoint.transform.position, this.SpawnPoint.transform.rotation);
                pool.Add(temp);
                temp.SetActive(false);
                temp.transform.SetParent(this.transform);
                temp.GetComponent<ObjetoMovimiento>().Initialice(this);
            }
            
        }
    }
    public void GetObject()
    {
        if (pool.Count > 0)
        {
            GameObject temp = pool[0];
            pool.Remove(temp);
            temp.SetActive(true);
            temp.GetComponent<ObjetoMovimiento>().SetPosition();
        }
    }
    public void SetObject(GameObject objeto)
    {
        //objeto.SetActive(false);
        if (pool.Count > 0)
        {
            GameObject temp = pool[0];
            pool.Add(temp);
        }
    }
    IEnumerator waveSpawner()
    {
        waveIsDone = false;
        GetObject();
        yield return new WaitForSeconds(spawnRate);
        waveIsDone = true;
    }
}
