﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BarquitoManager : MonoBehaviour
{
    public Pool pool1roca;
    public Pool pool2roca;
    public Pool pool3roca;
    public int contador = 0;
    public GameObject fondo_negro;
    public GameObject Inventario;
    public Image llave;
    public Image mapa;
    public Image libros;
    public Image ganzua;
    public Text text_Helps;
    public Text llaves_text;
    public Text mapas_text;
    public Text libros_text;
    public Text ganzua_text;
    public Text botontext;
    public barquito character;
    public Button cambioScena;
    private bool DistanciaMin = false;
    public string SceneName = "PantallaCarga";
    public bool showInventario = false;
    // Start is called before the first frame update
    void Awake()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        this.pool1roca.gameObject.SetActive(true);
        cambioScena.onClick.AddListener(() => ActivateLoadingCanvas());
    }

    // Update is called once per frame
    void Update()
    {
        contador++;
        if(this.contador == 1000)
        {
            this.pool2roca.gameObject.SetActive(true);
        }else if(this.contador == 2000)
        {
            this.pool3roca.gameObject.SetActive(true);
        }
        if(this.contador == 7000)
        {
            GeneralManager.instance.AumHelp();
            contador = 0;
        }
        if(this.contador == 5000)
        {
            this.DistanciaMin = true;
        }
        if(this.character.armadura < 1)
        {
            contador = 0;
            Morir();
            this.character.GetComponent<BoxCollider2D>().enabled = false;
            if(showInventario)
            {
                this.fondo_negro.SetActive(false);
            }
            else
            {
                this.fondo_negro.SetActive(true);
            }
        }
    }
    public void Morir()
    {
        int helps = GeneralManager.instance.GetHelp();
        if(helps > 0)
        {
            this.text_Helps.text = "Leíste " + GeneralManager.instance.GetHelp() + " libros durante el viaje, que inteligente!";
        }
        else
        {
            this.text_Helps.text = "El viaje fue tan turbulento que no pudiste leer, una pena";
        }
        
        if(this.DistanciaMin)
        {
            GeneralManager.instance.AumIndex(2);
            this.cambioScena.gameObject.SetActive(true);
        }
        else
        {
            this.SceneName = "barquitoUwU";
            this.text_Helps.text = "El barco se hundió antes de llegar al destino";
            this.botontext.text = "Reintentar";
            this.cambioScena.gameObject.SetActive(true);
        }
    }
    public void ShowInventario()
    {
        this.Inventario.SetActive(true);
        this.showInventario = true;
        this.mapas_text.text = GeneralManager.instance.GetMapa().ToString();
        this.llaves_text.text = GeneralManager.instance.GetLaves().ToString();
        this.libros_text.text = GeneralManager.instance.GetHelp().ToString();
        this.ganzua_text.text = GeneralManager.instance.GetGanz().ToString();
    }
    public void CerrarInventario()
    {
        this.Inventario.SetActive(false);
        this.showInventario = false;
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
