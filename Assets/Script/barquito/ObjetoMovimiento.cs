﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoMovimiento : MonoBehaviour
{
    public float speed;
    public Pool pool_manager;
    public GameObject SpawnPoint;
    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector2.left * speed * Time.deltaTime);
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Collider")
        {
            this.gameObject.SetActive(false);
            pool_manager.SetObject(this.gameObject);
        }
    }
    public void Initialice(Pool manager)
    {
        pool_manager = manager;
        this.SpawnPoint = manager.gameObject;
    }
    public void SetPosition()
    {
        Vector2 pos = new Vector2(this.SpawnPoint.transform.position.x, this.SpawnPoint.transform.position.y);
        this.transform.position = pos;
    }
}
