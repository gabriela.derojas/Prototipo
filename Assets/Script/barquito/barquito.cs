﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class barquito : MonoBehaviour
{
    Touch touch;
    private Vector2 first_press, second_press;
    public int position = 1;
    public bool IsDown = true;
    public int armadura = 3;
    public Text textArmadura;
    // Start is called before the first frame update
    public void Start()
    {
        this.textArmadura.text = this.armadura.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1)
        {
            touch = Input.GetTouch(0);
            switch(touch.phase)
            {
                case TouchPhase.Began:
                    first_press = touch.position;
                    break;
                case TouchPhase.Ended:
                    second_press = touch.position;
                    Vector2 direction = second_press - first_press;
                    if (direction.y > 0.5f && direction.y >= Mathf.Abs(direction.x) * 2 && this.position != 0)
                    {
                        //"subir"
                        this.position--;

                    }
                    else if (direction.y < -0.5f && direction.y <= -Mathf.Abs(direction.x) * 2 && this.position != 2)
                    {
                        //"bajar"
                        this.position++;
                    }
                    break;
            }
        }
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            this.position--;
        }else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            this.position++;
        }
        if (this.position == 0)
        {
            Vector2 new_position = new Vector2(-5.0f, 3.0f);
            this.transform.position = Vector2.MoveTowards(this.transform.position, new_position, 3.0f * Time.deltaTime);
        }
        else if (this.position == 1)
        {
            Vector2 new_position = new Vector2(-6.0f, 0f);
            this.transform.position = Vector2.MoveTowards(this.transform.position, new_position, 3.0f * Time.deltaTime);
        }
        else if (this.position == 2)
        {
            Vector2 new_position = new Vector2(-7.0f, -3.0f);
            this.transform.position = Vector2.MoveTowards(this.transform.position, new_position, 3.0f * Time.deltaTime);
        }
        
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Roca")
        {
            this.armadura--;
            this.textArmadura.text = this.armadura.ToString();
        }
    }
}
