﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class Puzzle1 : MonoBehaviour
{
    public Text respuesta_texto;
    public Text Resultado;
    private string Respuesta;
    private string letra;
    public Teclas[] teclas;
    public Button menu;
    public Button reintentar;
    public Button listo;
    public Button borrar;
    public string SceneName;
    public int cant_help;
    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        this.respuesta_texto.text = "";
        this.Respuesta = "MAGALLANES";
        menu.onClick.AddListener(() => ChangeName(menu));
        reintentar.onClick.AddListener(() => ChangeName(reintentar));
    }

    // Update is called once per frame
    void Update()
    {
        Escribir();
    }
    public void Escribir()
    {
        for(int i = 0; i < teclas.Length; i++)
        {
            if(teclas[i].press)
            {
                this.respuesta_texto.text = this.respuesta_texto.text + teclas[i].gameObject.name;
                teclas[i].press = false;
            }
        }
    }
    public void Borrar()
    {
        this.respuesta_texto.text = "";
    }
    public void Comparar()
    {
        if(this.respuesta_texto.text == this.Respuesta)
        {
            this.Resultado.text = "Felicidades";
        }
        else
        {
            Borrar();
            this.Resultado.text = "Incorrecto";
            this.cant_help = GeneralManager.instance.GetHelp();
            if(cant_help > 0)
            {
                this.respuesta_texto.text = "Quieres repasar el libro e intentar de nuevo?";
                this.reintentar.gameObject.SetActive(true);
                GeneralManager.instance.DisHelp();
            }
        }
        this.menu.gameObject.SetActive(true);
        this.listo.gameObject.SetActive(false);
        this.borrar.gameObject.SetActive(false);
        for (int i = 0; i < this.teclas.Length; i++)
        {
            this.teclas[i].gameObject.SetActive(false);
        }
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
    public void ChangeName(Button boton)
    {
        if (boton.gameObject.name == "Menu")
        {
            SceneName = "MainMenu";
            GeneralManager.instance.AumIndex(0);
            ActivateLoadingCanvas();
        }
        else if (boton.gameObject.name == "Reintentar")
        {
            SceneName = "Puzzles1";
            ActivateLoadingCanvas();
        }
    }
}
