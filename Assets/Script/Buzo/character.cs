﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class character : MonoBehaviour
{
    public bool stateUp;
    public bool stateDown;
    Touch touch;
    public float speed;
    private Vector2 first_press, second_press;
    private GameObject obj;
    public int Oxigeno;
    public GameObject camara;
    public bool getCofre = false;
    public int contador = 0;
    public BuzoManager manager;
    // Update is called once per frame
    void Update()
    {
        if(stateUp)
        {
            this.transform.position += Vector3.up * this.speed * Time.deltaTime;
            if(this.camara.transform.position.y < 1)
            {
                this.camara.transform.position += Vector3.up * this.speed * Time.deltaTime;
            }
        }
        if(stateDown)
        {
            this.transform.position -= Vector3.up * this.speed * Time.deltaTime;
            if(this.camara.transform.position.y > -14.8f)
            {
                this.camara.transform.position -= Vector3.up * this.speed * Time.deltaTime;
            }
        }
        if (Input.touchCount == 1)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                first_press = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                second_press = touch.position;
                Vector2 direction = second_press - first_press;
                if (direction.x > 0.5f && direction.x >= Mathf.Abs(direction.y) * 2)
                {
                    //state_text.text = "Derecha";
                    this.transform.position = Vector2.MoveTowards(this.transform.position, direction, 3.0f * Time.deltaTime);
                    if(this.camara.transform.position.x < 11.45f)
                    {
                        this.camara.transform.position += Vector3.right * this.speed * Time.deltaTime;
                    }
                }
                else if (direction.x < -0.5f && direction.x <= -Mathf.Abs(direction.y) * 2)
                {
                    //state_text.text = "Izquierda";
                    this.transform.position = Vector2.MoveTowards(this.transform.position, direction, 3.0f * Time.deltaTime);
                    if (this.camara.transform.position.x > -11.45f)
                    {
                        this.camara.transform.position += Vector3.left * this.speed * Time.deltaTime;
                    }
                }
            }
        }
        if(this.transform.position.y >= 8.3f)
        {
            this.stateUp = false;
        }
        if(this.contador == 900 && this.transform.position.y < 7.3)
        {
            this.Oxigeno = this.Oxigeno - 5;
            this.contador = 0;
        }
        if(this.Oxigeno < 1)
        {
            this.manager.Morir();
            this.gameObject.SetActive(false);
        }
        this.contador++;
        //controles PC
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Translate((speed * Time.deltaTime), 0, 0);
            if (this.camara.transform.position.x < 11.45f)
            {
                this.camara.transform.position += Vector3.right * this.speed * Time.deltaTime;
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Translate(-(speed * Time.deltaTime), 0, 0);
            if (this.camara.transform.position.x > -11.45f)
            {
                this.camara.transform.position += Vector3.left * this.speed * Time.deltaTime;
            }
        }
    }
    void disminuirOxigeno()
    {

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Tesoro")
        {
            collision.gameObject.transform.parent = this.gameObject.transform;
            this.obj = collision.gameObject;
            this.getCofre = true;
        }
        else
        {
            this.Oxigeno = this.Oxigeno - 10;
            if(this.transform.GetChild(0) != null)
            {
                this.obj.gameObject.transform.parent = null;
            }
            this.getCofre = false;
        }
    }
}
