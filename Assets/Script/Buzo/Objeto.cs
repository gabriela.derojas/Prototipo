﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objeto : MonoBehaviour
{
    public character pj;
    private Rigidbody2D rig_2d;
    // Start is called before the first frame update
    void Start()
    {
        rig_2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.parent == null)
        {
            this.rig_2d.gravityScale = 1;
        }
        else
        {
            this.rig_2d.gravityScale = 0;
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        /*objectMenuUi.SetActive(true);
        
        if(rnd == 0)
        {
            obj.sprite = llave;
            this.nombre_cosa.text = "Llave oxidada";
        }
        else if (rnd == 1)
        {
            obj.sprite = mapa;
            this.nombre_cosa.text = "Mapa Antiguo";
        }*/
        pj.stateDown = false;
        pj.stateUp = false;
    }
}
