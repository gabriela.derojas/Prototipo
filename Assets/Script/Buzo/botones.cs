﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class botones : MonoBehaviour,IPointerDownHandler, IPointerUpHandler
{
    public character pj;
    public Button button;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if(this.gameObject.name == "up")
        {
            pj.stateUp = true;
            pj.stateDown = false;
        }
        if (this.gameObject.name == "down")
        {
            pj.stateUp = false;
            pj.stateDown = true;
        }
        if (this.gameObject.name == "stop")
        {
            pj.stateUp = false;
            pj.stateDown = false;
        }
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        //no deberia moverse
    }
}
