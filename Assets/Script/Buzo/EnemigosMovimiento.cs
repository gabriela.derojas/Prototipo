﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigosMovimiento : MonoBehaviour
{
    public GameObject position1;
    public GameObject position2;
    public bool MoveIsDone = true;
    public int position = 1;
    public float speed;
    public bool direction = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(direction)
        {
            this.position = 2;
        }else if (!direction)
        {
            this.position = 1;
        }
        if (this.position == 1)
        {
            Vector2 new_position = new Vector2(this.position1.transform.position.x, this.position1.transform.position.y);
            this.transform.position = Vector2.MoveTowards(this.transform.position, new_position, this.speed * Time.deltaTime);
            if(this.transform.position.x == new_position.x)
            {
                direction = true;
            }
            
        }
        else if (this.position == 2)
        {
            Vector2 new_position = new Vector2(this.position2.transform.position.x, this.position2.transform.position.y);
            this.transform.position = Vector2.MoveTowards(this.transform.position, new_position, this.speed * Time.deltaTime);
            if (this.transform.position.x == new_position.x)
            {
                direction = false;
            }
        }
    }
}
