﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BuzoManager : MonoBehaviour
{
    public GameObject objectMenuUi;
    public GameObject InventarioUi;
    public Image cofre;
    public Image ganzua;
    public Text nombre_cosa;
    public Text llaves_text;
    public Text mapas_text;
    public Text helps_text;
    public Text ganzua_text;
    public Text MedidorOxigeno;
    public Text Titulo;
    public Text buttonText;
    public character pj;
    private bool flag = true;
    public Button cambioScena;
    public string SceneName = "";
    public GameObject cofreImagen;    
    public GameObject ganzuaImagen;   
    void Awake()
    {
        cambioScena.onClick.AddListener(() => ActivateLoadingCanvas());
    }

    // Update is called once per frame
    void Update()
    {
        if(pj.transform.position.y > 7.3f && this.flag && this.pj.getCofre)
        {
            objectMenuUi.SetActive(true);
            this.cofreImagen.SetActive(true);
            this.nombre_cosa.text = " Un cofre cerrado";
            GeneralManager.instance.AumIndex(3);
            this.SceneName = "PantallaCarga";
            if (this.pj.Oxigeno > 30)
            {
                GeneralManager.instance.AumGanz();
                this.ganzuaImagen.SetActive(true);
                this.ganzua.gameObject.SetActive(true);
                this.nombre_cosa.text = this.nombre_cosa.text + " y una ganzua";
            }
            this.flag = false;
        }
        this.MedidorOxigeno.text = this.pj.Oxigeno.ToString();
    }
    
    public void ShowInventario()
    {
        this.helps_text.text = GeneralManager.instance.GetHelp().ToString();
        this.ganzua_text.text = GeneralManager.instance.GetGanz().ToString();
        this.InventarioUi.SetActive(true);
        objectMenuUi.SetActive(false);
    }
    public void Cerrar()
    {
        this.InventarioUi.SetActive(false);
        objectMenuUi.SetActive(true);
    }
    public void Morir()
    {
        this.objectMenuUi.SetActive(true);
        this.cambioScena.gameObject.SetActive(true);
        this.SceneName = "SampleScene";
        this.buttonText.text = "Reintentar";
        this.nombre_cosa.text = "Por falta de oxigeno no pudiste llegar al cofre";
        this.Titulo.text = "Lástima";
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
