﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneralManager : MonoBehaviour
{
    public static int cant_llave;
    public static int cant_mapa;
    public static int cant_ganzuas;
    public static int cant_help;
    private static GeneralManager _instance;
    public static int LevelIndex = 0;
    // Start is called before the first frame update
    static public GeneralManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Object.FindObjectOfType(typeof(GeneralManager)) as GeneralManager;

                if (_instance == null)
                {
                    GameObject go = new GameObject("GeneralManager");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<GeneralManager>();
                }
            }
            return _instance;
        }
    }
    public void AumHelp()
    {
        cant_help++;
    }
    public void DisHelp()
    {
        cant_help--;
    }
    public int GetHelp()
    {
        return cant_help;
    }
    public void AumLlaves()
    {
        cant_llave++;
    }
    public void DisLlaves()
    {
        cant_llave--;
    }
    public int GetLaves()
    {
        return cant_llave;
    }
    public void AumMapa()
    {
        cant_mapa++;
    }
    public void DisMapa()
    {
        cant_mapa--;
    }
    public int GetMapa()
    {
        return cant_mapa;
    }
    public void AumGanz()
    {
        cant_ganzuas++;
    }
    public void DisGanz()
    {
        cant_ganzuas--;
    }
    public int GetGanz()
    {
        return cant_ganzuas;
    }
    public void AumIndex(int index)
    {
        LevelIndex = index;
    }
    public int GetIndex()
    {
        return LevelIndex;
    }
}
