﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Button StartButton;
    public Button BarcoButton;
    public Button BuzoButton;
    public Button BaseButton;
    public string SceneName = "";
    public GameObject Mapa;
    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        StartButton.onClick.AddListener(() => ShowMapa());
        BuzoButton.onClick.AddListener(() => ChangeName(BuzoButton));
        BarcoButton.onClick.AddListener(() => ChangeName(BarcoButton));
        BaseButton.onClick.AddListener(() => ChangeName(BaseButton));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {

        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone)
        {
            yield return null;
        }
    }
    public void ShowMapa()
    {
        this.Mapa.SetActive(true);
    }
    public void ChangeName(Button boton)
    {
        if (boton.gameObject.name == "BuzoBoton")
        {
            SceneName = "PantallaCarga";
            GeneralManager.instance.AumIndex(2);
            ActivateLoadingCanvas();
        }
        else if (boton.gameObject.name == "BarcoBoton")
        {
            SceneName = "PantallaCarga";
            GeneralManager.instance.AumIndex(1);
            ActivateLoadingCanvas();
        }
        else if (boton.gameObject.name == "BaseBoton")
        {
            SceneName = "PantallaCarga";
            GeneralManager.instance.AumIndex(3);
            ActivateLoadingCanvas();
        }
    }
}
