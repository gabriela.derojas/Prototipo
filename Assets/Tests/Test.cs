﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
public class Test
{
    public string respuesta_texto = "MAGALLANES";
    public string Respuesta = "MAGALLANES";
    public bool correcta =  false;
    [UnityTest]
    public IEnumerator RespuestaCorrecta()
    {
        if(respuesta_texto == Respuesta)
        {
            correcta = true;
        }
        if(correcta)
        {
            Assert.AreEqual(this.Respuesta, this.respuesta_texto);
        }
        else
        {
            Assert.AreNotEqual(this.Respuesta, this.respuesta_texto);
        }
        yield break;
    }
    /*public GameObject obj;
    [UnityTest]
    public IEnumerator NoHayPadre()
    {
        SetUpScene();
        if(obj.gameObject.transform.parent == null)
        {
            Assert.IsNull(obj.gameObject.transform.parent);
        }
        yield break;
    }
    void SetUpScene()
    {
        obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("cofre"));
    }*/
    /*public int cant_objetosBonus = 1;
    public bool assertsCheck;
    [UnityTest]
    public IEnumerator TienesObjetos()
    {
        if (cant_objetosBonus > 0)
        {
            assertsCheck = true;
            Assert.IsTrue(assertsCheck);
        }else
        {
            assertsCheck = false;
            Assert.IsFalse(assertsCheck);
        }
        yield break;
    }*/
}
